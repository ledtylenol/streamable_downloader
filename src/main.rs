use std::{env, fs, io::ErrorKind};

use anyhow::Result;
use serde::Deserialize;
use tokio::{fs::File, io::AsyncWriteExt};
#[derive(Deserialize)]
struct StreamableVideo {
    files: StreamableFiles,
    title: String,
}

#[derive(Deserialize)]
struct StreamableFiles {
    mp4: StreamableFile,
}

#[derive(Deserialize)]
struct StreamableFile {
    url: String,
}
#[tokio::main]
async fn main() -> Result<()> {
    let file_name = env::args().nth(1).unwrap_or("links.txt".into());
    let pool_count = env::args()
        .nth(2)
        .unwrap_or("10".into())
        .parse::<usize>()
        .unwrap_or(10);
    let folder = env::args().nth(3).unwrap_or("videos".into());
    if let Err(e) = fs::create_dir(&folder) {
        if e.kind() != ErrorKind::AlreadyExists {
            return Err(e.into());
        }
    };
    let video_links = fs::read_to_string(file_name)?
        .lines()
        .map(|line| line.to_owned())
        .collect::<Vec<_>>();
    let mut tasks = vec![];
    for links in video_links.chunks(pool_count) {
        for link in links {
            let task = tokio::spawn(download_file(link.clone(), folder.clone()));
            tasks.push(task);
        }
        for (i, task) in &mut tasks.iter_mut().enumerate() {
            let res = task.await;
            match &res {
                Err(e) => eprintln!("Something went wrong: {}", e),
                Ok(_) => println!("{i}"),
            }
        }
        tasks.clear();
    }
    Ok(())
}

async fn download_file(url: String, folder: String) -> Result<()> {
    let shortcode = url.split('/').last().unwrap();
    let api_url = format!("https://api.streamable.com/videos/{}", shortcode);

    let response = reqwest::get(&api_url).await?;

    if response.status().is_success() {
        let video_info: StreamableVideo = response.json().await?;

        let mp4_url = video_info.files.mp4.url;
        let title = match video_info.title.rsplit_once('.') {
            Some((left, right)) => format!("./{}/{}{}.mp4", folder, left, right),
            None => format!("./{}/{}.mp4", folder, video_info.title),
        };

        let video_content = reqwest::get(&mp4_url).await?.bytes().await?;

        let mut file = File::create(&title).await?;

        file.write_all(&video_content).await?;
    }
    Ok(())
}
