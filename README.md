# streamable_downloader

jank streamable downloader that downloads streamable videos from streamable (true and real) from a file and sets the file name to the title of the video
seems to fail if the video doesn't have a title, whoops (not that it matters, if a video fails it will simply skip over them)
oh well

pretty simple to use, you open it like any other terminal app, 
give it the relative path to the file and optionally the number of parallel threads to use

(unfortunately streamable does not seem to provide any API for automatically getting all videos from a specific user so you have to paste them in manually anyway)
